#  ---------------------- Описание работы модуля в целом -----------------------------------
# Модуль получает значения из таблиц MSSQL и обновляет данных Postgres в соответсвие с конфигурацией 
# модуля scheme.py. Для лучшего понимания работы модуля, необходимо вначале ознакомиться с модулем chemes.py.
# запуск модуля осуществляется командой: python manage.py get_base_update.py
# Модуль состоить из следующих функций:
# handle - основная функция
# process_data - функция обновления данных таблиц
# get_data - получение данных из MSSQL и сохранение их в словарь new_data_dict
# save_param_data - обновление данных базовых и итоговых таблиц
# прочие вспомогательные функции.


import sys, os, logging, traceback, pyodbc as db
import base.management.commands.scheme as scheme
from base.management.commands.scheme import simple_param_classes, complex_param_classes, TABLES_SCHEME, DATA_PACKAGING_SCHEME, CODE_SIGN
from django.core.management.base import BaseCommand
from django.db import models
from dotenv import load_dotenv
from base.management.commands.generate_price.generate_price import main


'''---------------Настройка подключение к БД------------'''
load_dotenv()
dsn = os.environ.get('dsn')
user = os.environ.get('user')
password = os.environ.get('password')
database = os.environ.get('database')

'''---------------Настройка логгирования------------'''
log_path = 'logs/1c_base_sync/'
if not os.path.exists(log_path):
    os.makedirs(log_path)
logging.basicConfig(level=logging.INFO, filename=f'{log_path}sync.log', filemode="a", 
    format="%(asctime)s %(levelname)s %(message)s")

new_data = {}

class Command(BaseCommand):
    help = 'Get new data from 1C Tables via DB connect'

    # запуск скрипта обновления python manage.py get_updates

    def handle(self, *args, **options) -> None:
        logging.info('Start base update')
        process_data()
        logging.info('Update successful')
        # запускаем файл generate_price - обновление прайс-листа
        main(new_data)


# Словарь guid_map служит для хранения сопоставленных пар значение - ключ для используемых моделей (таблиц)
# Смысл словаря состоим в том чтобы при цикличном проходе по базовым моделям сразу вносить данные пар значение - ключ
# в словарь, чтобы в будущем при заполнение итоговых таблиц исключить поиск этих значений в базовых таблицах
# Данный подход заимстовован из модуля get_updates.py и позволяет существенно повысить скорость обработки данных.

# Структура словаря выглядит следующим образом
#     {
#         'Metro': {'Автозаводская': 12, ...}, 
#         'Diameter': {'12': 16, '13': 17, '14': 18, ...}, 
#         'Season': {'з': 5, 'л': 6}, 
#         'LoadIndex': {'100': 129, '101': 130, ...},
#         ...
#     }

guid_map = {}


# Сохранение значений в базу
# Функция проходит циклично по всем таблицам, обновляя их значения
# data - список значений вида [{'guid': 'abc', 'val': '111'}, ...]. Для итоговых таблиц data получает 
# уже отфильтрованные значения по параметру CODE_SIGN в зависимости от маркировки Артикула
# klass - модель, куда сохранять, например, TireWidth
# При использовании функции обновляется общие кэш guid'ов: guid_map.update(save_param_data(data, klass))

def save_param_data(new_data: dict, klass: object) -> dict:
    # Выставляем метку кода в Артикуле для шин или дисков (Т или W) и отфильтровываем эти значения из data
    guid_cache = {}
    count = 1
    if klass.__name__ in CODE_SIGN.keys():
        cs = CODE_SIGN.get(klass.__name__)
        data = dict(filter(lambda item: cs in item[1]['code'], new_data['Products'].items()))
    else:
        data = new_data.get(klass.__name__, [])
    
    if not data:
        return guid_cache
    guid_dict = {}
    for guid_key, item in data.items():
        if is_guid_empty(guid_key):
            continue
        # собираем словарь для обновления модели (табл.)
        fields = klass._meta.get_fields()
        upd_data = {}
        for field in fields:
            if 'width' in field.name:# поскольку поля 'width' и 'width_rear' называются одинаково и для шин и для дисков
                if klass.__name__ == 'Tire':
                    ud = item.get('width_tire') if field.name == 'width' else item.get('width_rear_tire')
                else:
                    ud = item.get('width_disc') if field.name == 'width' else item.get('width_rear_disc')
            else:
                ud = item.get(field.name)
            if ud != None:
                # Для связанных полей, id которых находим по guid
                if isinstance(field, models.fields.related.ForeignKey):
                    # для базовых таблиц берем связанные значения id из таблиц   
                    if klass in simple_param_classes:
                        mod = field.related_model
                        id_data = get_id(bin_to_guid(ud, klass.__name__), mod)
                        if id_data:
                            upd_data['{}_id'.format(field.name)] = id_data
                    else:
                        mod_name = field.related_model.__name__
                        guid_list = guid_map.get(mod_name)
                        if guid_list != None:
                            if guid_list.get(ud) != None:
                                upd_data['{}_id'.format(field.name)] = guid_list.get(ud)
                else:
                    upd_data[field.name] = ud
        if klass in simple_param_classes:
            obj, created = klass.objects.update_or_create(guid = guid_key, defaults=upd_data)
            # Добавляем id в guid_cache
            # Получаем guid_cache вида {'Model': {'guid': guid_key, 'id': obj.id}}
            upd_dict = {}
            id_param_name = DATA_PACKAGING_SCHEME[klass.__name__][1].get('id_param_name')
            if id_param_name:
                upd_dict =  {getattr(obj, id_param_name): obj.id}
            else:
                upd_dict =  {obj.name: obj.id} 
            guid_dict.update(upd_dict)
        else:
            # Таблицу Fasteners заполняем по отдельному алгоритму
            if klass.__name__ == 'Fasteners':
                # удаляем данные, которые не требуют обновления
                guid_val = upd_data.pop('guid')
                store_val = upd_data.pop('store_id')
                obj, created = klass.objects.update_or_create(guid = guid_val, store_id = store_val, defaults=upd_data)
            else:
                obj, created = klass.objects.update_or_create(guid = guid_key, defaults=upd_data)
            # логгируем запись об обновлении каждых 10 000 строк
            if count % 10000 == float(0):
                logging.info("Таблица {}, обновлено {} записей".format(klass.__name__, count))
            count += 1
    if klass in simple_param_classes:
        guid_cache[klass.__name__] = guid_dict
        return guid_cache
    else:
        # логгируем запись об обновлении всех строк
        logging.info("Таблица {}, обновлено {} записей".format(klass.__name__, count))
        return {}


def get_id(guid: str, klass: object) -> int:
    obj_ids = klass.objects.filter(guid=guid).values_list('id', flat=True)
    obj_id = obj_ids[0] if obj_ids else None
    return obj_id

# получаем class obj по его имени
def kls_name_to_cls(kls_name: str) -> object:
    kls = None
    try:
        kls = getattr(scheme, kls_name)
    except:
        pass
    return kls

def bin_to_guid(data_bin: object, kls_name: str) -> str:
    global simple_param_classes
    st = data_bin.hex()
    kls = kls_name_to_cls(kls_name)
    if kls in simple_param_classes:
        
        n = 4 #блоки разбиваются по 4
        guidtuple = tuple(st[n * f:n * (f + 1)] for f in range(8))
        order = (7, 8, 6, 5, 1, 2, 3, 4)
        ng = tuple(guidtuple[x - 1] for x in order)
        return f"{ng[0]}{'-'.join(ng[1:6])}{''.join(ng[6:])}"
    else:
        return '{}-{}-{}-{}-{}'.format(st[:8], st[8:12], st[12:16], st[16:20], st[20:])

def is_guid_empty(guid: str) -> bool:
    if guid == '00000000-0000-0000-0000-000000000000' or not guid:
        return True
    return False


# get_data - получение данных из MSSQL и сохранение их в словарь new_data_dict
# Алгоритм работы следующий:
# Получаем данные из MSSQL циклом из таблиц, указанных в DATA_PACKAGING_SCHEME
# полученные guid переводим из bytes в str
# обновляем поля табл. 'Products' данными из табл. 'Stocks'

def get_data() -> dict:
   
    con_string = 'DSN=%s;UID=%s;PWD=%s;DATABASE=%s;' % (dsn, user, password, database)
    try:
        connect = db.connect(con_string)
    except:
        logging.error(traceback.format_exc())
        logging.critical('Exit as a result of an error')
        sys.exit()

    ''' ------- Создаём запрос ------------------------'''
    new_data_dict = {}
    for scheme, tables in TABLES_SCHEME.items():
        cursor = connect.cursor()
        for table in tables:
            fields = []
            param_names = []
            model_table = ''
            for tab_name, val in DATA_PACKAGING_SCHEME.items():
                for param in val:
                    if param.get('1C_table') == table:
                        fields.append(param.get('1C_field'))
                        param_names.append(param.get('param_name'))
                        # получаем имя модели, сопоставленное с данной 1С таблицей
                        if model_table != tab_name:
                            model_table = tab_name
            # обновление данных из 1С таблиц
            if scheme != 'requests':
                if table == 'Products':
                    qstr = "SELECT {} FROM [{}].[{}] WHERE Номенклатура_Артикул LIKE 'T%' or Номенклатура_Артикул LIKE 'W%'".format(', '.join(fields), scheme, table)
                elif table == 'Справочники.Склады':
                    qstr = "SELECT {} FROM [{}].[{}] WHERE ЭтоГруппа <> cast(00 as binary(32))".format(', '.join(fields), scheme, table)
                else:
                    qstr = "SELECT {} FROM [{}].[{}]".format(', '.join(fields), scheme, table)
            # обновление данных из 1С запросов, например для табл. "Крепёж" (goods_common_fasteners)
            else:
                qstr = tables.get(table)
            connect.timeout = 60# выставляем таймаут 1 мин
            try:
                rst = cursor.execute(qstr).fetchall()
            except:
                logging.error(traceback.format_exc())
                logging.critical('Exit as a result of an error')
                sys.exit()
            data_dict = {}
            if scheme != 'requests':
                for row in rst:
                    col_dict = {}
                    for index, val in enumerate(param_names[1:]):
                        col_dict[val] = row[index + 1]
                    row_0 = bin_to_guid(row[0], model_table) if isinstance(row[0], bytes) else row[0]
                    data_dict[row_0.lower()] = col_dict
            else:
                for num, row in enumerate(rst):
                    col_dict = {}
                    for index, val in enumerate(param_names):
                        col_dict[val] = row[index] if val != 'guid' else row[index].lower()
                    data_dict[num] = col_dict
                
            new_data_dict[model_table] = data_dict
    # обновляем поля табл. 'Products'
    products = new_data_dict.get('Products')
    stocks = new_data_dict.get('Stocks')
    reserves = new_data_dict.get('Dispatches')

    for key in products:
        stocks_data = stocks.get(key)
        if stocks_data:
            # защита от значений null в 1С Stocks
            if stocks_data.get('cost') is None:
                stocks_data.update({'cost': 0})
            try:
                if float(stocks_data.get('balance')) < 0:# защита от ошибок БД 1С с отрицательным значением balance
                    continue
            except:
                logging.warning(traceback.format_exc())
            products[key].update(stocks_data)
        else:
            products[key].update({'store': None, 'balance': 0, 'cost': 0})

        # обновляем данные о количестве и резерве
        reserve_data = reserves.get(key)
        reserve = reserve_data.get('reserve', 0) if reserve_data else 0
        products[key].update({'reserve': reserve})
        balance = products[key].get('balance')
        if balance:
            count = balance - reserve
            products[key].update({'count': count})
    return new_data_dict

# process_data - функция обновления данных таблиц. Скорее номинальная фукция, в которой получаем данные и циклом проходим по всем базовым и итоговым таблицам
# вся логика обновления содержится в функции save_param_data
def process_data() -> None:
    global new_data
    new_data = get_data()
   
    for klass in (simple_param_classes + complex_param_classes):
        guid_map.update(save_param_data(new_data, klass))

