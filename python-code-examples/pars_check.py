import datetime
import numpy as np
import os
import pandas as pd
import sys

from django import db
from django.core.management.base import BaseCommand
from multiprocessing import Pool

from tire.search_and_calc import search_and_calc_cost
from tire.check_functions import check_log, get_models_data
from tire.models import Cost


class Command(BaseCommand):
    help = '''Проверка результатов парсинга. Аргументы: -db дата парсинга в БД (таблица tire_cost поле date_load)
                пример параметра: -db 2022-09-15, если аргумент отсутсвует, проверяем последнюю дату парсинга в БД;
                -prod продукция которую проверяем, если значение -prod all, то проверяем всю продукцию, 
                иначе, если аргумент prod отсутсвует - только по условию Количество > 0 and Резерв = 0;
                -n_jobs параметр мультипроцессинга, разделяет артикулы на пачки и параллельно вычисляет результат;
                пример команды: python manage.py pars_check -db 2022-09-15 -prod all -n_jobs 2
            '''

    def add_arguments(self, parser) -> None:
        parser.add_argument('-db', type=str)
        parser.add_argument('-prod', type=str)
        parser.add_argument('-n_jobs', type=int, default=2)

    def handle(self, *args, **options) -> None:
        db = options['db']
        prod = options['prod']
        prod_all = False
        parsdate = pars_date_list()
        if db:
            # проверяем что указанная и предыдущая дата загрузки существуют          
            try:
                fol_date_ind = parsdate.index(db)
            except:
                fol_date_ind = None
            if fol_date_ind is None:
                # здесь и ниже вывод делаем в командную сроку, поэтому используется print
                print(f'В таблице tire_cost отсутсвуют данные на указанную дату {db}. Exit pars_check')
                sys.exit()
            else:
                last_cost = db
        else:
            last_cost = parsdate[0]
        if prod:
            if prod != 'all':
                print(f'Аргумент -prod не распознан {prod}. Exit pars_check')
                sys.exit()
            else:
                prod_all = True
        check_log.info('------------------ Начало проверки парсинга ----------------------')
        models = get_models_data(prod_all)
        analyse(models, last_cost, n_jobs=options['n_jobs'])


# функция отдает список всех имеющихся дат парсинга
def pars_date_list() -> str:
    pars_date = Cost.objects.all().order_by('-date_load').distinct('date_load').values_list('date_load', flat=True)
    parsdate = [dat.strftime("%Y-%m-%d") for dat in pars_date]
    return parsdate


def analyse_chunk(models_list: list, lastcost: str) -> object:
    num = len(models_list)
    analys_df = pd.DataFrame({
        'Артикул': pd.Series(dtype='str'),
        'Q1': pd.Series(dtype='int'),
        'Q2': pd.Series(dtype='int'),
        'Q3': pd.Series(dtype='int'),
        'Q4': pd.Series(dtype='int'),
        'Q5': pd.Series(dtype='int'),
        'Стоимость_расчётная': pd.Series(dtype='float'),
        'Стоимость_существующая': pd.Series(dtype='float'),
        'Разница%': pd.Series(dtype='float'),
        # добавляем параметры для упрощения анализа
        'Количество': pd.Series(dtype='int'),
        'Сезонность': pd.Series(dtype='str'),
        'Шипы': pd.Series(dtype='bool'),
        'Ширина_ПередняяОсь': pd.Series(dtype='str'),
        'Ширина_ЗадняяОсь': pd.Series(dtype='str'),
        'ПрофильПередняяОсь': pd.Series(dtype='str'),
        'ПрофильЗадняяОсь': pd.Series(dtype='str'),
        'Радиус': pd.Series(dtype='str'),
        'Усиленная': pd.Series(dtype='bool'),
        'Ранфлэт': pd.Series(dtype='bool'),
        'Премиальность': pd.Series(dtype='int'),
        'Брэнд': pd.Series(dtype='str'),
        'Модель': pd.Series(dtype='str'),
        'Год': pd.Series(dtype='str'),
        'Износ': pd.Series(dtype='str')
    })
    for ind, model in enumerate(models_list):
        try:
            count = model.get('count')
            season = 'w' if model.get('season') == 'з' else 's'
            is_studded = model.get('is_studded')
            width_front = model.get('width_front', '0')
            width_rear = model.get('width_rear', '0')
            aspect_ratio_front = model.get('aspect_ratio_front', '0')
            aspect_ratio_rear = model.get('aspect_ratio_rear', '0')
            diameter = model.get('diameter', '0')
            is_runflat = model.get('is_runflat')
            is_reinforced = model.get('is_reinforced')
            premium = model.get('premium')
            brand = model.get('brand')
            tire_model = model.get('tire_model')
            year = model.get('year')
            wear = model.get('wear')
            cost = model.get('cost')
        except Exception as e:
            check_log.error(f'Problem with product: {model}')
            check_log.error(e)
            continue

        # получаем рассчитанную стоимость + глобальную переменную
        # с результатами поиска количества на каждом этапе из search_tires_cost
        cost_res_tuple = search_and_calc_cost(
            count=count,
            season=season,
            is_studded=is_studded,
            width_front=width_front,
            width_rear=width_rear,
            aspect_ratio_front=aspect_ratio_front,
            aspect_ratio_rear=aspect_ratio_rear,
            diameter=diameter,
            is_runflat=is_runflat,
            is_reinforced=is_reinforced,
            premium=premium,
            brand=brand,
            tire_model=tire_model,
            year=year,
            wear_front=wear,
            wear_rear=None,
            parsing_check=True,
            lastcost=lastcost
        )

        # сохрянем стоимость
        cost_chk = cost_res_tuple[0]
        # сохрянем переменную res для результатов поиска позиций
        res = cost_res_tuple[1]
        # высчитываем разницу в ценах
        calccost = cost_chk.get('cost')
        if calccost is not None:
            calccost = calccost / count
            calccost = round(calccost / 50) * 50  # округление до 50
            if cost and calccost:
                diffcost = round(((calccost - cost) / calccost * 100), 4)
            else:
                diffcost = 0
        else:
            calccost = 0
            diffcost = 0
        code = model.get('code')
        res.insert(0, code)
        res += [
            calccost, cost, diffcost, count, season, is_studded, width_front, width_rear,
            aspect_ratio_front, aspect_ratio_rear, diameter, is_reinforced,
            is_runflat, premium, brand, tire_model, year, wear
        ]
        analys_df.loc[len(analys_df)] = res
        # логгируем запись об обновлении каждых 1000 строк
        if ind % 1000 == 0 and ind != 0:
            check_log.info(f"Проанализировано записей {ind} из {num}")

    return analys_df


def analyse(models_list: list, lastcost: str, n_jobs: int=2) -> None:
    # Во избежание ошибки закрываем все соединения
    # каждый поток создает собственное соединение
    db.connections.close_all()
    # Запускаем анализ в многопоточном режиме
    check_log.info(f'Got {len(models_list)} records. Splitting it in {n_jobs} chunks.')
    chunks = np.array_split(models_list, n_jobs)
    with Pool(n_jobs) as pool:
        res_df = pool.starmap(analyse_chunk, zip(chunks, [lastcost] * n_jobs))
    analys_df = pd.concat(res_df, ignore_index=True)

    # экспорт в excel
    now = datetime.datetime.now()
    current_time = now.strftime("%H-%M")
    cur_date = now.strftime("%Y-%m-%d")
    fname = f'analyse_DB_date_{lastcost}_chk_date_{cur_date}_{current_time}'
    # создаем папку для лог-файла
    res_path = 'logs/parsing_analyses_results'
    if not os.path.exists(res_path):
        os.makedirs(res_path)
    analys_df.to_excel(f'{res_path}/{fname}.xlsx')
    check_log.info(f'Анализ парсинга успешно завершён.')
    check_log.info(f'Подробные результаты экспортированы в файл {res_path}/{fname}.xlsx')
    check_log.info('Общие результаты:')
    # рассчитываем общие результаты
    calc_col = ('Q1', 'Q2', 'Q3', 'Q4', 'Q5')
    for col in calc_col:
        colsum = (analys_df[col] != 0).mean(axis=0)
        perc = round(colsum * 100, 4)
        check_log.info(f'Параметр {col}, общий процент найденных значений {perc} %')
    # анализ разницы цен товара последнего и предыдущего парсинга
    mean_diff = round(analys_df['Разница%'].abs().mean(), 4)
    median_diff = round(analys_df['Разница%'].abs().median(), 4)
    check_log.info(f'Среднее значение изменения расчётной и существующей цены: {mean_diff}%')
    check_log.info(f'Медианное значение изменения расчётной и существующей цены: {median_diff}%')
    check_log.info(f'Количество проанализированных позиций: {analys_df.shape[0]}')
