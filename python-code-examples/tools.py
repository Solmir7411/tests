
import sys, traceback
from django.core.management.base import BaseCommand
from tire.common_classes import LoggingInfo
from tire.models import TireModel, Tire, Cost
from tire.check_functions import separator_change

class Command(BaseCommand):
    help = '''Вспомогательные функции для работы с парсингом. 
                Аргументы: -c команда, которая исполняeт указанную в ней функцию. 
                Например: tools -c mod_prem_ref выполнит функцию mod_prem_ref()
            '''
    def add_arguments(self, parser) -> None:
        parser.add_argument('-c', type = str)

    def handle(self, *args, **options) -> None:
        func_name = options['c']
        Tools()._call_func(func_name)


class Tools():
    def __init__(self) -> None:
        log_cls = LoggingInfo()
        self.tools_log = log_cls.get_logger('logs', 'tools_log.log')

	# call_func возвращает фукцию по имени для возможности работы с ней
    def _call_func(self, f_name) -> object:
        try:
            func = getattr(self, f_name)
        except:
            self.tools_log.error(traceback.format_exc())
            self.tools_log.critical('Exit as a result of an error')
            sys.exit()
        return func()
    
    # рефакторинг таблицы tire_tiremodel, 
    # оставляем только одну модель с уникальным name_cluster
    def mod_prem_ref(self) -> None:
        # выбираем уникальные name_cluster моделей
        cluster_list = TireModel.objects.all().order_by('name_claster').distinct('name_claster').values_list('name_claster', flat=True)
        for c in cluster_list:
            # выбираем модель у которой должна быть премиальность и делаем её эталонной
            clset = TireModel.objects.filter(name_claster = c).order_by('premium')
            ref_model = clset.first()
            ref_model_id = ref_model.id
            del_clset = clset.exclude(id = ref_model_id)
            no_ref_mod = tuple(del_clset.values_list('id', flat=True))
            Tire.objects.filter(tire_model_id__in = no_ref_mod).update(tire_model_id = ref_model_id)
            del_clset.delete()
        # далее "подчищаем" табл. Tire_tire
        self.tire_ref()

    # рефакторинг таблицы tire_tire, 
    # оставляем только одну модель с уникальным tire_model_id
    def tire_ref(self) -> None:
        # выбираем уникальные name_cluster моделей
        tire_list = Tire.objects.all().order_by(
            'tire_model_id',
            'width',
            'aspect_ratio',
            'diameter',
            'season',
            'is_studded',
            'is_runflat',
            'reinforced',
            'load_index',
            'speed_rating',
        ).distinct(
            'tire_model_id',
            'width',
            'aspect_ratio',
            'diameter',
            'season',
            'is_studded',
            'is_runflat',
            'reinforced',
            'load_index',
            'speed_rating',
        ).values(
            'tire_model_id',
            'width',
            'aspect_ratio',
            'diameter',
            'season',
            'is_studded',
            'is_runflat',
            'reinforced',
            'load_index',
            'speed_rating',
        )
        for item in tire_list:
            # выбираем первую модель и делаем её эталонной
            tset = Tire.objects.filter(
                tire_model_id=item['tire_model_id'],
                width=item['width'],
                aspect_ratio=item['aspect_ratio'],
                diameter=item['diameter'],
                season=item['season'],
                is_studded=item['is_studded'],
                is_runflat=item['is_runflat'],
                reinforced=item['reinforced'],
                load_index=item['load_index'],
                speed_rating=item['speed_rating'],
            )
            if tset.count() == 1:
                continue
            ref_model = tset.first()
            ref_model_id = ref_model.id
            del_clset = tset.exclude(id = ref_model_id)
            no_ref_mod = tuple(del_clset.values_list('id', flat=True))
            Cost.objects.filter(tire_id__in = no_ref_mod).update(tire_id = ref_model_id)
            del_clset.delete()

    # меняем разделитель с ; на , в csv для колибровочных коэфф.
    def _sep_change(self):
        fpath = 'data_coef/calibration_coef.csv'
        separator_change(fpath)