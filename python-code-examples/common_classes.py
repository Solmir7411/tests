import os, logging, traceback, sys, re, pyodbc as db, psycopg2 as pdb
from dotenv import load_dotenv

# Класс logging_info позволяет логгировать в разные логи
# с разными настройками и директориями
class LoggingInfo():
    # для задания уникального имени логгера
    NUM_OBJ = 0

    def __init__(self) -> None:
        # инициализация основных настроек логгирования
        self.formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

    def get_logger(self, log_path: str, log_file: str, level: object=logging.INFO) -> object:
        LoggingInfo.NUM_OBJ += 1
        # создаем папку для лог-файла
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        # инициализация нового логгера
        file = os.path.join(log_path, log_file)
        self.handler = logging.FileHandler(file)
        self.handler.setFormatter(self.formatter)
        name = LoggingInfo.get_name()
        self.logger = logging.getLogger(name)
        self.logger.setLevel(level)
        self.logger.addHandler(self.handler)
        return self.logger

    @classmethod
    def get_name(cls) -> int:
        return f'logger{cls.NUM_OBJ}'

# класс - соединение к БД MS-SQL
class MssqlConnect():
    def __init__(self, log_file: str = None) -> None:
        if log_file:
            self.log_path, self.log_file = os.path.split(log_file)
        else:
            self.log_path = os.path.abspath(__file__) + '/logs/'
            self.log_file = 'db_connect.log'
        # Логгирование сообщений
        self.logger = LoggingInfo().get_logger(self.log_path, self.log_file)
        #---------------Настройка подключение к БД------------
        load_dotenv()
        self.dsn = os.environ.get('dsn')
        self.user = os.environ.get('user')
        self.password = os.environ.get('password')
        self.database = os.environ.get('database')
    
        con_string = 'DSN=%s;UID=%s;PWD=%s;DATABASE=%s;' % (self.dsn, self.user, self.password, self.database)
        try:
            self.connect = db.connect(con_string)
            #self.logger.info('MssqlConnect init successfull')
        except:
            self.logger.error(traceback.format_exc())
            self.logger.critical('Exit as a result of an error')
            sys.exit()
    def get_connect(self) -> object:
        return self.connect

    def get_logger(self) -> object:
        return self.logger

# класс - соединение к БД PSQL 
# пример использования класса
# db_cls = PsqlConnect('logs/get_cost/error.log')
# con = db_cls.get_connect()
# cur = con.cursor()
# cur.execute("select * from tire_tire where code = 'T0021684'")
# c = cur.fetchall()
# print(c)
class PsqlConnect():
    def __init__(self, log_file = None) -> None:
        if log_file:
            self.log_path, self.log_file = os.path.split(log_file)
        else:
            self.log_path = os.path.abspath(__file__) + '/logs/'
            self.log_file = 'db_connect.log'
        # Логгирование сообщений
        self.logger = LoggingInfo().get_logger(self.log_path, self.log_file)
        #---------------Настройка подключение к БД------------
        load_dotenv()
        host = os.environ.get('host')
        dbname = os.environ.get('dbname')
        user = os.environ.get('user_psql')
        password = os.environ.get('password_psql')

        conn_string = f"host={host} dbname={dbname} user={user} password={password}"
        try:
            self.connect = pdb.connect(conn_string)
            #self.logger.info('PsqlConnect init successfull')
        except:
            self.logger.error(traceback.format_exc())
            self.logger.critical('Exit as a result of an error')
            sys.exit()
    def get_connect(self):
        return self.connect

    def get_logger(self):
        return self.logger

